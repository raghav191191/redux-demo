import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";

class Child1 extends Component {
  render() {
    return (
      <div className="App">
        <div className="Age-label"> This is Child1
          your age: <span>{this.props.age}</span>
        </div>
        <button onClick={this.props.onAgeUp}>Age UP</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    age: state.age
  };
};

const mapDispachToProps = dispatch => {
  return {
    onAgeUp: () => dispatch({ type: "AGE_UP", value: 1 }),
  };
};
export default connect(
  mapStateToProps,
  mapDispachToProps
)(Child1);
