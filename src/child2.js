import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";

class Child2 extends Component {
  render() {
    return (
      <div className="App">
        <div className="Age-label"> This is Child2
          your age: <span>{this.props.age}</span>
        </div>
       
        <button onClick={this.props.onAgeDown}>Age Down</button> 
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    age: state.age
  };
};

const mapDispachToProps = dispatch => {
  return {
    onAgeDown: () => dispatch({ type: "AGE_DOWN", value: 1 })
  };
};
export default connect(
  mapStateToProps,
  mapDispachToProps
)(Child2);
